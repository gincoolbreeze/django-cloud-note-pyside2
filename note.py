# -*- coding : utf-8 -*-

from PySide2.QtWidgets import QApplication, QWidget, QLabel, QMainWindow, QDockWidget,QPlainTextEdit
from PySide2.QtCore import Qt
from PySide2.QtGui import QIcon,QMovie

import sys
import time
import requests

from ui.ui_home import Ui_MainWindow
from windows.mdi_list_note import MdiHomeWindow
from windows.add_note import MdiAddWindow
from windows.login import LoginWindow

from session.note_session import SessionConnet


class WindowMain(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        
        self.debug_win = None
        
        # 会话身份信息
        self.session = session
        
        self.mdi_home_window = None
        self.mdi_add_window = None
        self.pushButton_home.clicked.connect(self.pushButton_home_clicked)
        self.pushButton_add.clicked.connect(self.pushButton_add_clicked)
        self.pushButton_home.setCursor(Qt.PointingHandCursor)
        self.pushButton_add.setCursor(Qt.PointingHandCursor)
        
        self.action_login.triggered.connect(self.login)
        self.action_debug.triggered.connect(self.debug)
        
        # 日志信息
        self.loginfo = QPlainTextEdit()
        self.loginfo.document().setMaximumBlockCount(1000)
    
    def pushButton_home_clicked(self):
        if self.mdi_home_window is None:
            self.mdi_home_window = MdiHomeWindow(self.session, parent=self)
            self.mdiArea.addSubWindow(self.mdi_home_window)
            self.mdi_home_window.show()
        else:
            try:
                self.mdi_home_window.showMaximized()
            except Exception:
                self.mdi_home_window = MdiHomeWindow(self.session, parent=self)
                self.mdiArea.addSubWindow(self.mdi_home_window)
                self.mdi_home_window.show()

    def pushButton_add_clicked(self):
        if self.mdi_add_window is None:
            self.mdi_add_window = MdiAddWindow(self.session)
            self.mdiArea.addSubWindow(self.mdi_add_window)
            self.mdi_add_window.show()
        else:
            try:
                self.mdi_add_window.showMaximized()
            except Exception:
                self.mdi_add_window = MdiAddWindow(self.session)
                self.mdiArea.addSubWindow(self.mdi_add_window)
                self.mdi_add_window.show()
    
    def login(self):
        "登录"
        window = LoginWindow(self.session)
        window.show()
        window.exec()
        
    def debug(self):
        self.debug_win = QDockWidget("调试窗口", self)
        self.debug_win.setWidget(self.loginfo)
        self.debug_win.setFloating(True)
        self.addDockWidget(Qt.BottomDockWidgetArea, self.debug_win)
        self.debug_win.show()

session = SessionConnet()
# session.session = {"sessionid": "i0is7u64727jcayv2a1ado8q7w0rnk0f"}

# 开始运行
app = QApplication(sys.argv)
winMain = WindowMain()
winMain.show()
# winMain.setMouseTracking(True) # 鼠标跟随
app.exec_()