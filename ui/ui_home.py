# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'homesaEUsa.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(800, 600)
        self.action_select_directory = QAction(MainWindow)
        self.action_select_directory.setObjectName(u"action_select_directory")
        self.action_login = QAction(MainWindow)
        self.action_login.setObjectName(u"action_login")
        self.action_logout = QAction(MainWindow)
        self.action_logout.setObjectName(u"action_logout")
        self.action_tile = QAction(MainWindow)
        self.action_tile.setObjectName(u"action_tile")
        self.action_cascade = QAction(MainWindow)
        self.action_cascade.setObjectName(u"action_cascade")
        self.action_close_all = QAction(MainWindow)
        self.action_close_all.setObjectName(u"action_close_all")
        self.action_debug = QAction(MainWindow)
        self.action_debug.setObjectName(u"action_debug")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.formLayout = QFormLayout(self.centralwidget)
        self.formLayout.setObjectName(u"formLayout")
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.pushButton_home = QPushButton(self.centralwidget)
        self.pushButton_home.setObjectName(u"pushButton_home")
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_home.sizePolicy().hasHeightForWidth())
        self.pushButton_home.setSizePolicy(sizePolicy)
        self.pushButton_home.setMinimumSize(QSize(0, 50))

        self.verticalLayout.addWidget(self.pushButton_home)

        self.pushButton_add = QPushButton(self.centralwidget)
        self.pushButton_add.setObjectName(u"pushButton_add")
        self.pushButton_add.setMinimumSize(QSize(0, 50))

        self.verticalLayout.addWidget(self.pushButton_add)

        self.pushButton = QPushButton(self.centralwidget)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setMinimumSize(QSize(0, 50))

        self.verticalLayout.addWidget(self.pushButton)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)


        self.formLayout.setLayout(0, QFormLayout.LabelRole, self.verticalLayout)

        self.mdiArea = QMdiArea(self.centralwidget)
        self.mdiArea.setObjectName(u"mdiArea")

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.mdiArea)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 800, 23))
        self.menu_file = QMenu(self.menubar)
        self.menu_file.setObjectName(u"menu_file")
        self.menu_account = QMenu(self.menubar)
        self.menu_account.setObjectName(u"menu_account")
        self.menu = QMenu(self.menubar)
        self.menu.setObjectName(u"menu")
        self.menu_2 = QMenu(self.menubar)
        self.menu_2.setObjectName(u"menu_2")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.toolBar = QToolBar(MainWindow)
        self.toolBar.setObjectName(u"toolBar")
        MainWindow.addToolBar(Qt.TopToolBarArea, self.toolBar)

        self.menubar.addAction(self.menu_file.menuAction())
        self.menubar.addAction(self.menu_account.menuAction())
        self.menubar.addAction(self.menu.menuAction())
        self.menubar.addAction(self.menu_2.menuAction())
        self.menu_file.addSeparator()
        self.menu_file.addAction(self.action_select_directory)
        self.menu_file.addSeparator()
        self.menu_account.addSeparator()
        self.menu_account.addAction(self.action_login)
        self.menu_account.addSeparator()
        self.menu_account.addAction(self.action_logout)
        self.menu_account.addSeparator()
        self.menu.addSeparator()
        self.menu.addAction(self.action_tile)
        self.menu.addSeparator()
        self.menu.addAction(self.action_cascade)
        self.menu.addSeparator()
        self.menu.addAction(self.action_close_all)
        self.menu.addSeparator()
        self.menu_2.addAction(self.action_debug)
        self.menu_2.addSeparator()
        self.toolBar.addAction(self.action_select_directory)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.action_tile)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.action_cascade)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.action_close_all)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.action_debug)
        self.toolBar.addSeparator()

        self.retranslateUi(MainWindow)
        self.action_tile.triggered.connect(self.mdiArea.tileSubWindows)
        self.action_cascade.triggered.connect(self.mdiArea.cascadeSubWindows)
        self.action_close_all.triggered.connect(self.mdiArea.closeAllSubWindows)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"\u4e91\u7b14\u8bb0", None))
        self.action_select_directory.setText(QCoreApplication.translate("MainWindow", u"\u9009\u62e9\u5de5\u4f5c\u76ee\u5f55", None))
#if QT_CONFIG(tooltip)
        self.action_select_directory.setToolTip(QCoreApplication.translate("MainWindow", u"\u9009\u62e9\u5de5\u4f5c\u76ee\u5f55", None))
#endif // QT_CONFIG(tooltip)
        self.action_login.setText(QCoreApplication.translate("MainWindow", u"\u767b\u5f55", None))
#if QT_CONFIG(tooltip)
        self.action_login.setToolTip(QCoreApplication.translate("MainWindow", u"\u767b\u5f55", None))
#endif // QT_CONFIG(tooltip)
        self.action_logout.setText(QCoreApplication.translate("MainWindow", u"\u9000\u51fa", None))
#if QT_CONFIG(tooltip)
        self.action_logout.setToolTip(QCoreApplication.translate("MainWindow", u"\u9000\u51fa", None))
#endif // QT_CONFIG(tooltip)
        self.action_tile.setText(QCoreApplication.translate("MainWindow", u"\u5e73\u94fa\u7a97\u53e3", None))
#if QT_CONFIG(tooltip)
        self.action_tile.setToolTip(QCoreApplication.translate("MainWindow", u"\u5e73\u94fa\u7a97\u53e3", None))
#endif // QT_CONFIG(tooltip)
        self.action_cascade.setText(QCoreApplication.translate("MainWindow", u"\u7ea7\u8054\u7a97\u53e3", None))
#if QT_CONFIG(tooltip)
        self.action_cascade.setToolTip(QCoreApplication.translate("MainWindow", u"\u7ea7\u8054\u7a97\u53e3", None))
#endif // QT_CONFIG(tooltip)
        self.action_close_all.setText(QCoreApplication.translate("MainWindow", u"\u5173\u95ed\u6240\u6709\u7a97\u53e3", None))
#if QT_CONFIG(tooltip)
        self.action_close_all.setToolTip(QCoreApplication.translate("MainWindow", u"\u5173\u95ed\u6240\u6709\u7a97\u53e3", None))
#endif // QT_CONFIG(tooltip)
        self.action_debug.setText(QCoreApplication.translate("MainWindow", u"\u6253\u5f00\u8c03\u8bd5\u7a97\u53e3", None))
#if QT_CONFIG(tooltip)
        self.action_debug.setToolTip(QCoreApplication.translate("MainWindow", u"\u6253\u5f00\u8c03\u8bd5\u7a97\u53e3", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_home.setText(QCoreApplication.translate("MainWindow", u"\u4e3b\u9875", None))
#if QT_CONFIG(tooltip)
        self.pushButton_add.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.pushButton_add.setText(QCoreApplication.translate("MainWindow", u"\u6dfb\u52a0\u7b14\u8bb0", None))
        self.pushButton.setText(QCoreApplication.translate("MainWindow", u"PushButton", None))
        self.menu_file.setTitle(QCoreApplication.translate("MainWindow", u"\u6587\u4ef6", None))
        self.menu_account.setTitle(QCoreApplication.translate("MainWindow", u"\u8d26\u53f7", None))
        self.menu.setTitle(QCoreApplication.translate("MainWindow", u"\u7a97\u53e3\u64cd\u4f5c", None))
        self.menu_2.setTitle(QCoreApplication.translate("MainWindow", u"\u8c03\u8bd5", None))
        self.toolBar.setWindowTitle(QCoreApplication.translate("MainWindow", u"toolBar", None))
    # retranslateUi

