# -*- coding : utf-8 -*-

from PySide2.QtWidgets import QApplication, QDialog, QHeaderView, QMessageBox
from PySide2.QtCore import Qt, QThread, Signal

import sys
import time

from ui.ui_update_note import Ui_Dialog


class UpdateNoteWindow(QDialog, Ui_Dialog):
    def __init__(self, session, note_id, parent=None):
        self.parent = None
        self.session = session
        self.note_id = note_id
        super().__init__()
        self.task_post = None
        self.task_flag = True # 可以运行
        self.note_update_url = 'http://127.0.0.1:8008/note/update/?id={id}'.format(id=self.note_id)
        self.setupUi(self)
        self.pushButton_cancel.clicked.connect(self.cancle_widget_data)
        self.pushButton_update.clicked.connect(self.submit_widget_data)
        self.pushButton_close.clicked.connect(self.close)
        
        
        
    def cancle_widget_data(self):
        self.lineEdit_title.setText("")
        self.textBrowser.setText("")
    
    def submit_widget_data(self):
        title = self.lineEdit_title.text()
        content = self.textBrowser.toPlainText()
        data_dict = {
            'title': title,
            'content': content,
        }
        if self.task_flag:
            self.task_post = PostData(self.session, data_dict, self.note_update_url)
            self.task_post.signal_done[str].connect(self.show_add_result)
            self.task_post.start()
            self.task_flag = False
        
    def show_add_result(self, string):
        if string == "ok":
            QMessageBox.information(self, '信息', '更新成功！', QMessageBox.Ok)
            self.close()
        else:
            QMessageBox.information(self, '信息', '更新失败！', QMessageBox.Ok) 
        self.task_flag = True
    

# 提交数据
class PostData(QThread):
    
    signal_done = Signal(str)
    
    def __init__(self, session, data, url):
        super().__init__()
        self.session = session
        self.data = data
        self.url = url
    
    def run(self):

        resp = None
        
        try:
            resp = self.session.requests.post(
                self.url,
                cookies = self.session.session,
                data = self.data,
            )
        except Exception as e:
            self.signal_done.emit(str(e))
        else:
            if resp.content.decode() == 'OK':
                self.signal_done.emit('ok')
            else:
                self.signal_done.emit('error')


if __name__ == "__main__":
    # 开始运行
    import requests
    app = QApplication(sys.argv)
    winMain = MdiAddWindow(requests)
    winMain.show()
    # winMain.setMouseTracking(True) # 鼠标跟随
    app.exec_()